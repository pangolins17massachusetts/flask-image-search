// ----- custom js ----- //
//<script src="pangolin_util.js" type="text/javascript"></script>


// hide initial
$("#searching").hide();
$("#results-table").hide();
$("#error").hide();

// global
var base_url = 'http://209.6.125.190:8080/';
var base_image_url = base_url + 'static/'
var data = [];


function basename(path) {
    return path.replace(/\\/g,'/').replace( /.*\//, '' );
}
     
function dirname(path) {
    return path.replace(/\\/g,'/').replace(/\/[^\/]*$/, '');;
}

function basename_noextension(path) {
    return path.replace(/\.[^/.]+$/, "")
}

$(function() {
    // sanity check
    console.log("ready!");

    function do_query_image(img_url) {
        $("#results").empty();
        $("#results-table").hide();
        $("#error").hide();
       // if (img_url) {
       //     $("#query_image").attr('src', img_url)
       //     $("#query_image").attr('title', img_url)
       //     console.log(img_url)
       // }

        // show searching text
        $("#searching").show();
        console.log("searching...")
            // ajax request

        var cur_dataset = $('input[name=dataset]:checked').val(); 

        $.ajax({
            type: "POST",
            url: "/search",
            data: { img: img_url, dataset:cur_dataset},
            // handle success
            success: function(result) {
                console.log(result.results);
                var data = result.results
                    // show table
                $("#searching").hide();
                $("#query_image").attr('src', img_url)
                $("#query_image").attr('title', img_url)

                $("#results-table").show();
                // loop through results, append to dom
                for (i = 0; i < data.length; i++) {
                    var dname = dirname(data[i]["image"])
                    var fname = basename(data[i]["image"])
                    fname = basename_noextension(fname)
                    //console.log(dname)
                    //console.log(fname)
                    var result_url = base_image_url + data[i]["image"]
                    var audio_url1 = base_image_url + dname + '/' + fname + '.ogg'
                    var audio_url2 = base_image_url + dname + '/' + fname + '.m4a'           
                    $("#results").append('<tr><th><img src="' + result_url + '" class="result-img">' + '<p><audio preload="none" class="audio_desc"> <source src="' + audio_url1 + '" type="audio/ogg">' + '<source src="' + audio_url2 + '"> Sorry... </audio></p>' + '</th><th>' + data[i]['score'] + '</th><th>' + data[i]["image"] + '</th></tr>')
                };
            },
            // handle error
            error: function(error) {
                console.log(error);
                // append to dom
                $("#error").append()
            }
        });
    }

    // ajax updates content. to make it clickable, we need to bound the event to
    // the closest static item (!!!!)
    // image click
    //$(".img").click(function() {
    $(".row").on("click", ".img", function(e) {
        //prevent double firing
        e.preventDefault();
        e.stopImmediatePropagation();

        // remove active class
        $(".img").removeClass("active")
            // add active class to clicked picture
        $(this).addClass("active")

        // grab image url
        var image = $(this).attr("src")
            //alert(image)
        do_query_image(image)

        return false
    });

    $("#url_submit").click(function() {

        // grab image url
        var image = $("#query_url").val()
        do_query_image(image)
    });

    $("#upload_image").change(function() {
        var file_data = $("#upload_image").prop("files")[0];
        var form_data = new FormData();
        form_data.append("file", file_data)
        var cur_dataset = $('input[name=dataset]:checked').val(); 
        form_data.append("dataset", cur_dataset)
        $.ajax({
            type: "POST",
            url: "/search",
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            // handle success
            success: function(result) {
                console.log(result.results);
                var data = result.results
                    // empty previous search results
                $("#results").empty();
                var url = base_url + result.url
                console.log(url)
                $("#query_image").attr('src', url)
                $("#query_image").attr('title', url)
                // show table
                $("#searching").hide();
                $("#results-table").show();
                // loop through results, append to dom
                for (i = 0; i < data.length; i++) {
                    var dname = dirname(data[i]["image"])
                    var fname = basename(data[i]["image"])
                    fname = basename_noextension(fname)
                    //console.log(dname)
                    //console.log(fname)
                    var result_url = base_image_url + data[i]["image"]
                    var audio_url1 = base_image_url + dname + '/' + fname + '.ogg'
                    var audio_url2 = base_image_url + dname + '/' + fname + '.m4a'           
                    $("#results").append('<tr><th><img src="' + result_url + '" class="result-img">' + '<p><audio preload="none" class="audio_desc"> <source src="' + audio_url1 + '" type="audio/ogg">' + '<source src="' + audio_url2 + '"> Sorry... </audio></p>' + '</th><th>' + data[i]['score'] + '</th><th>' + data[i]["image"] + '</th></tr>')
                };
            },
            // handle error
            error: function(error) {
                console.log(error);
                // append to dom
                $("#error").append()
            }
        });
    });

    function goto_page(pnum) {
        // ajax request
        var cur_dataset = $('input[name=dataset]:checked').val(); 
        $.ajax({
            type: "POST",
            url: "/goto_page",
            data: { page_num: pnum, dataset:cur_dataset},
            // handle success
            success: function(result) {
                console.log(result.results);
                var data = result.results;
                $("#query_image_page").empty();
                $("#query_image_page").append('<tr>');
                for (i = 0; i < data.length; i++) {
                    $("#query_image_page").append('<th><img src="' + base_image_url + data[i] +
                        '" class="img"></th>')
                };
                $("#query_image_page").append('</tr>');
            },
            // handle error
            error: function(error) {
                console.log(error);
                // append to dom
                $("#error").append()
            }
        });
    }

    // $(".goto_page").on('click', function(e) {
    $(".row").on("click", ".goto_page", function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        var pnum = $(this).text();
        console.log(pnum);

        goto_page(pnum)
        return false;
    });

    // $(".goto_page").on('click', function(e) {
    $(".row").on("click", ".goto_view", function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        var page_roll = $(this).text().trim();
        console.log(page_roll);
        
        var cur_dataset = $('input[name=dataset]:checked').val(); 
        // ajax request
        $.ajax({
            type: "POST",
            url: "/goto_view",
            data: { roll: page_roll, dataset:cur_dataset},
            // handle success
            success: function(result) {
                console.log(result.results);
                var data = result.results;
                $("#page_list").empty()
                $("#page_list").append('<a href="#" class="goto_view"><<</a> ')
                $("#page_list").append('<a href="#" class="goto_view"><</a> ')
                for (i = data[0]; i <= data[1]; i++) {
                    $("#page_list").append('<a href="#" class="goto_page">' + i + '</a> ')
                };
                $("#page_list").append('<a href="#" class="goto_view">></a> ')
                $("#page_list").append('<a href="#" class="goto_view">>></a>')
                    // now go to the first page of this view
                goto_page(data[2])
            },
            // handle error
            error: function(error) {
                console.log(error);
                // append to dom
                $("#error").append()
            }
        });

        return false;
    });


    $(".table").on("mouseenter", ".result-img", function() {
        //    alert('sss')
        $(this).addClass('transition');
        // find the audio content
        //var p = $(this).parent()
        //var audio = p.find("audio")[0]
       // var audio = $("#audio_desc")[0]
        //audio.play()

        //  }, function() {
        //    $(this).removeClass('transition');
    }).on("mouseleave", ".result-img", function() {
        $(this).removeClass('transition');
        // find the audio content
        var p = $(this).parent()
        var audio = p.find("audio")[0]
        if (!audio.paused) {
            audio.pause()
        }
    });
    
    $(".table").on("click", ".result-img", function() {
        //    alert('sss')
        var p = $(this).parent()
        var audio = p.find("audio")[0]
       // var audio = $("#audio_desc")[0]
        if (audio.paused) {
            audio.play()
        }
    });
   
    // switch dataset 
   // $(".row").on("click", ".database", function() {
    $("input[name=dataset]:radio").click(function() {
        if ($(this).checked)
            return false

        var dataset = $(this).val()
        $("#results").empty();
        $("#results-table").hide();
        $("#error").hide();
        $("#query_image").attr('src', '')
        $("#query_image").attr('title', '')
        window.location.href = base_url + dataset;
        return true;
    });
});
