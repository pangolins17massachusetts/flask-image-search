import os
from easydict import EasyDict as edict
from imagesearch.searcher import Searcher, cosine_distance
from features.colordescriptor import ColorDescriptor
from features.cnndescriptor import CNNDescriptor
from features.feature_io import load_model

__C = edict()

cfg = __C

#configuration start here
__C.USE_PCA = False
__C.IMG_FEATURE_TYPE = 'cnn'
__C.DATA_INDEX_DIR = '../../data/index'
__C.VISUAL_FEATURE_DIR = '../../data/visual_features'
__C.IMG_UPLOAD_DIR = 'static/upload'
# web page related parameters
__C.PANGOLIN_SERVER_IP='209.6.125.190'
__C.NUM_TOP_HITS = 10
__C.IMG_PER_PAGE = 10
__C.PAGE_PER_VIEW = 20

PANGOLIN_DATASETS = {}

class DataSet:
    def __init__(self, name, index_dir, img_per_page = 10, page_per_view = 10):
       self.query_img_index = []
       self.tot_query_img = 0
       self.current_page_num = 1
       self.max_page_num = 0
       self.max_view_num = 0
       self.name = name
       self.descriptor = None
       self.searcher = None

       self._init_dataset(name, index_dir, img_per_page, page_per_view)

    def _load_index_file(self, filename):
        with open(filename) as f:
           img_list = f.readlines()
        img_list = [x.strip() for x in img_list] 
        return img_list        

    def _init_dataset(self, name, index_dir, img_per_page, page_per_view):

        query_index_file = os.path.join(index_dir, name, 'query_index.txt')
    #    print query_index_file
        self.query_img_index = self._load_index_file(query_index_file)
        self.tot_query_img = len(self.query_img_index)
        assert(self.tot_query_img)

        # maximum number of pages for query images
        self.max_page_num = self.tot_query_img / img_per_page 
        if self.tot_query_img % img_per_page:
            self.max_page_num = self.max_page_num + 1
    
        self.max_view_num = self.max_page_num / page_per_view
        if self.max_page_num % page_per_view:
            self.max_view_num = self.max_view_num + 1

        print "==== creating dataset: " + name + " ===="

    def set_searcher(self, feat_dir, feat_type):
       index_feat_path = os.path.join(feat_dir, self.name, feat_type+'_index.csv')
       self.searcher = Searcher(index_feat_path, cosine_distance)

    def get_descriptor(self):
        if self.descriptor is None:
            self.descriptor = CNNDescriptor()

        return self.descriptor

if __name__ == '__main__':
    d = DataSet('museum', cfg.DATA_INDEX_DIR)
    d.set_searcher(cfg.VISUAL_FEATURE_DIR, cfg.IMG_FEATURE_TYPE)
    print d.query_img_index
    print d.tot_query_img
