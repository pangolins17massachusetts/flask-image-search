import os

from flask import Flask, render_template, request,  jsonify
from urlparse import urlparse, urljoin
from werkzeug.urls import url_fix
from flask_uploads import (UploadSet, configure_uploads, IMAGES,
                           UploadNotAllowed, UploadConfiguration)

#from imagesearch.searcher import Searcher, cosine_distance, chi2_distance
from config import cfg, DataSet, PANGOLIN_DATASETS

import numpy as np
from skimage import io
import cv2
import traceback

# create flask instance
app = Flask(__name__)

# Image uploader
app.config['UPLOADED_PHOTOS_DEST'] = cfg.IMG_UPLOAD_DIR
uploaded_photos = UploadSet('photos', IMAGES)
configure_uploads(app, uploaded_photos)

# check if the image is hosted on the same server. If so, strip off the
# host name and lave the relative path only


def fix_image_url(img_url):

    url_info = urlparse(img_url)
    if (url_info.netloc).split(':')[0] == cfg.PANGOLIN_SERVER_IP:
        new_url = (url_info.path).strip('/')
    else:
        # replace spaces by '%20'
        new_url = url_fix(img_url)

    return new_url

# main route

@app.route('/')
@app.route('/<dname>')
def index(dname='museum'):
   # initialize the dataset
   try:
       if dname not in PANGOLIN_DATASETS:
           PANGOLIN_DATASETS[dname] = DataSet(dname, cfg.DATA_INDEX_DIR, cfg.IMG_PER_PAGE, cfg.PAGE_PER_VIEW)
           PANGOLIN_DATASETS[dname].set_searcher(cfg.VISUAL_FEATURE_DIR, cfg.IMG_FEATURE_TYPE)
   except:
        return jsonify({"sorry": "Sorry, failed to create dataset"}), 500

   dataset = PANGOLIN_DATASETS[dname]
   k = min(cfg.IMG_PER_PAGE, dataset.tot_query_img)
   return render_template('index.html', query_images=dataset.query_img_index[0:k], num_pages=min(dataset.max_page_num, cfg.PAGE_PER_VIEW), dataset_name=dname)

@app.route('/goto_view', methods=['POST'])
def goto_view():

    if request.method == "POST":
        # get url
        roll = request.form.get('roll')
        dname = request.form.get('dataset')

        advance = 0
        if roll == '<':
            advance = - 1
        elif roll == '<<':
            advance = - 2
        elif roll == '>':
            advance = 1
        elif roll == '>>':
            advance = 2
        else:
            return jsonify({"sorry": "Whoops, wrong button."}), 500
        
        dataset = PANGOLIN_DATASETS[dname]
        current_view = int((dataset.current_page_num - 1) / cfg.PAGE_PER_VIEW)
        new_view = min(max(current_view + advance, 0), dataset.max_view_num - 1)
        sid = new_view * cfg.PAGE_PER_VIEW + 1
        eid = min(sid+cfg.PAGE_PER_VIEW - 1, dataset.max_page_num)

        # view changed, reset current page number
        if current_view != new_view:
            dataset.current_page_num = sid
        print '==== ' + roll + ','+str(dataset.max_page_num) + ','+str(dataset.max_view_num) + ',(' + str(current_view) + ',' + str(new_view) + ',' + str(sid) + ',' + str(eid) + ')' + ' ===='
        # return success
        return jsonify(results=[sid, eid, dataset.current_page_num])



@app.route('/goto_page', methods=['POST'])
def goto_page():
    if request.method == "POST":
        # get url
        page_num = int(request.form.get('page_num'))
        dname = request.form.get('dataset')
        dataset = PANGOLIN_DATASETS[dname]

        dataset.current_page_num = page_num
        sid = (page_num - 1) * cfg.IMG_PER_PAGE
        eid = min(sid + cfg.IMG_PER_PAGE, dataset.tot_query_img)
   #     print '====' + str(cfg.CURRENT_PAGE_ID) + str(sid) + ' ' + str(eid) + '===='
   #     print cfg.QUERY_IMG_INDEX[sid:eid]
        # return success
        return jsonify(results=dataset.query_img_index[sid:eid])


@app.route('/search', methods=['POST'])
def search():
    if request.method == "POST":

        RESULTS_ARRAY = []

        # get url
        print request.form
        print request.files
        if 'file' in request.files:
            filename = uploaded_photos.save(request.files['file'])
            image_url = uploaded_photos.path(filename)
        elif 'img' in request.form:
            image_url = request.form.get('img')
        else:
            return jsonify({"sorry": "Wrong input!"}), 500

        dname = request.form['dataset']
        print dname
        # fix url, otherwise it failed to load image
        image_url = fix_image_url(image_url)
        try:
            print image_url
            
            dataset = PANGOLIN_DATASETS[dname]

            dc = dataset.get_descriptor()
            features = dc.extract_feature(image_url)

            # perform the search
            #results = searcher.search(features[0,:])
            results = dataset.searcher.search(features, cfg.NUM_TOP_HITS)

            # loop over the results, displaying the score and image name
            for (resultID, score) in results:
                RESULTS_ARRAY.append(
                    {"image": str(resultID), "score": str(score)})

            # return success
           # return jsonify(results=(RESULTS_ARRAY[::-1][:NUM_TOP_HITS]))
            return jsonify(results=RESULTS_ARRAY[:cfg.NUM_TOP_HITS], url=image_url)

        except Exception, e:
            traceback.print_exc()
            # return error
            return jsonify({"sorry": "Sorry, no results! Please try again."}), 500

# run!
if __name__ == '__main__':
    app.run('192.168.0.3', port=8080, debug=True)
